# README #

### What is this repository for? ###

* **Quick summary:** To compute temporal alignments among different recordings that are 
  all the same length but have different initial offsets from the beginning of each recording.
* **History and motivation:** We had several family members at different locations around the country 
  record a song while listening by earphone to the same recorded accompaniment.
  But they started their recording at various times prior to the start of
  the accompaniment. So their recordings are at the same tempo throughout but
  temporally offset. The goal of this project is to automatically compute
  the best duration to shift each recording by to stack them best so that
  no one sounds like s/he is rushing or dragging. Then we can enter that time
  shift in Audacity for each track. At the same time we can select a good volume
  adjustment to have the loudness of each track end up similar.
* **version 0.9**

### How do I get set up? ###

* ~~Summary of set up~~
* ~~Configuration~~
* Thanks to [this source](https://sound.stackexchange.com/questions/35501/how-can-i-extract-the-volume-from-an-mp3-or-mp4)
  for the reference to the [loudness_scanner](https://github.com/jiixyj/loudness-scanner) 
  repository.
* Dependencies: Python 3, Numpy, [loudness_scanner](https://github.com/jiixyj/loudness-scanner)
* ~~How to run tests~~
* ~~Deployment instructions~~

### Contribution guidelines ###

* I don't know, just let me know / submit a pull request if you find this 
  useful, find errors, or want to suggest improvements.

### Who do I talk to? ###

* Kevin Black
